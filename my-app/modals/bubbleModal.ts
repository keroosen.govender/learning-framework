export class LfNode {
    RoleDescription: any;
    RoleId?: string;
    Children: LfNodeWithChildren[];

    constructor() {
        this.Children = [];
    }
}

export class LfNodeWithChildren {
    JD1?: string = '';
    JD2?: string = '';
    JD3?: string = '';
    ThemeDescription?: string = '';
    ThemeId?: string ;
    D1?: string = '';
    ParentId?: number;
    Children?: LfNodeWithChildren[];
}