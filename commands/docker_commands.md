# Learning Framework Image Neo4j Image Build 

## Building an image

1.) Start a new instance of the container using the base container as the reference image

This base container has been prepared using a standard neo4j image and docker-copyedit.py to remove the volumes
  a) ./docker-copyedit.py FROM neo4j:3.5.3 INTO neo4j:3.5.3b REMOVE ALL VOLUMES
  b) Then rename the image by doing a docker tag
    docker tag neo4j:3.5.3b registry.gitlab.com/kaleidocode-assessments/kaleidocode_learning_frameworks/neo4j_base:latest


  docker run --name kc-tester-lfw -p 7474:7474 -p 7687:7687 -d registry.gitlab.com/kaleidocode-assessments/kaleidocode_learning_frameworks/neo4j_base

2.) Browse the UI of neo4j at http://localhost:7474/browser/
3.) Login using user name neo4j password neo4j
4.) Authenticate and change the password to admin
5.) Start nginx if not already started 
6.) place new csv files in ./learning/kc_learning_framework folder
7.) run graph_db scripts in the numbered order
8.) export the contents on the container to a new image using the docker commit command
  docker commit kc-tester-lfw registry.gitlab.com/kaleidocode-assessments/kaleidocode_learning_frameworks/kc-tester-lfw:latest
9.) login to the learning framework repo
  docker login registry.gitlab.com/kaleidocode-assessments/kaleidocode_learning_frameworks
10.) push the docker image to the kc repo
  docker push registry.gitlab.com/kaleidocode-assessments/kaleidocode_learning_frameworks/kc-tester-lfw:latest

**Note** - rename the image with a docker tag command